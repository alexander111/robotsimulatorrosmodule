#!/usr/bin/env python

import simulation
import sys
import rospy

if __name__ == '__main__':
    """
    The main function called when simulator.py is run
    from the command line:

    > python simulator.py
    """



    rospy.init_node('talker', anonymous=True)
    Targets = rospy.get_param('~Targets')
    Obstacles = rospy.get_param('~Obstacles')

    sim = simulation.Simulation(Targets, Obstacles)

    sim.run()




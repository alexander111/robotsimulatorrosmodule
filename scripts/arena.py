#!/usr/bin/env python

import math

class Arena(object):
    '''
    An environment holds the size of the arena and the robots inside them, with its
    positions and directions. It also contains the time spending in the arena and the state of the simulation
    '''
    def __init__(self, width = 20, height = 20):
        '''
        Default constructor of the size of the arena
        '''
        self.height = height
        self.width  = width
        self.targetRadius = 1
        self.obstacleRadius = 5
        
        self.win_line = ((0,0),(self.width,0))
        self.loose_line = ((0,self.height),(self.width,self.height))
        self.virtual_lines = [((0,0),(0,20)),((0,10),(20,10)),((20,0),(20,20))] 
        
    
    def getWinLine(self):
        return self.win_line
    
    def getLooseLine(self):
        return self.loose_line
    
    def getVirtualLines(self):
        return self.virtual_lines
    
    def dronePose(self):
        '''
        Place the robots and the line references inside the arena, acording to the initial state
        '''
        return ((0,0,1), 270) 
        
    def targetPoses(self, n_targetR = 10):
        '''
        Place the robots and the line references inside the arena, acording to the initial state
        '''
        targetRdirs  = [math.radians(x*(360/n_targetR)) for x in range(n_targetR)]
        targetRpos = [(self.width/2 + (self.targetRadius * math.cos(x)), self.height/2 + (self.targetRadius * math.sin(x)), 0) for x in targetRdirs]
        return [(targetRpos[i], targetRdirs[i], i) for i in range(n_targetR)]
       
    
    def obstaclePoses(self, n_obstacleR = 4):
        '''
        Place the robots and the line references inside the arena, acording to the initial state
        '''
        obstacleRdirs = [math.radians(x*(360/n_obstacleR)) for x in range(n_obstacleR)]
        obstacleRpos = [(self.width/2 + (self.obstacleRadius * math.cos(x)), self.height/2 + (self.obstacleRadius * math.sin(x)), 0) for x in obstacleRdirs]
        return [(obstacleRpos[i], obstacleRdirs[i]-(3*math.pi/2), i) for i in range(n_obstacleR)]
        
    def leaveRight(self, state):
        x, y, z = state.getPosition()

        return y < 0
        
    def leaveWrong(self, state):
        x, y, z = state.getPosition()
        return x < 0 or x > self.width or y > self.height or z < 0 or z > 3
    
if __name__ == '__main__':
    """
    The main function called when simulator.py is run
    from the command line:

    > python simulator.py

    See the usage string for more details.

    > python simulator.py --help
    """
    env = Arena()
    pass        
